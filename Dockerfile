#Getting base image
FROM ubuntu

MAINTAINER kyle keitz <keitzkyle@gmail.com>

ARG DEBIAN_FRONTEND=noninteractive
ADD build /tmp/build
ADD assets /tmp/assets
ADD terraform /tmp/terraform
ADD python /tmp/python

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y apt-utils debconf-utils dialog

# Perform updates on all packages
RUN apt-get update
RUN apt-get install -y apt-utils debconf-utils dialog
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN echo "resolvconf resolvconf/linkify-resolvconf boolean false" | debconf-set-selections
RUN apt-get update
RUN apt-get install -y resolvconf

# Install Unique Binaries
RUN bash /tmp/build/install-packages.sh
# RUN bash /tmp/build/aws-cli-update.sh
RUN bash /tmp/build/configure-locale-timezone.sh
RUN bash /tmp/build/generate-all-locales.sh
RUN bash /tmp/build/configure-core-dump-directory.sh
RUN bash /tmp/build/configure-firstboot.sh
RUN bash /tmp/terraform/terraform-install.sh
RUN bash /tmp/python/install-python.sh

# Remove the /tmp/* files
RUN rm -rf /tmp/*
