# Homework Week 2

## docker-image-builder

This repo is used for building and maintaining a Docker image to be used within other GitLab Pipelines.

The goal of this pipeline is to automate the build and update of the Docker image that is used throughout your team's pipelines.

## The pipeline jobs to build, test, and promote the Docker image consist of

- build and push the image release candidate (tagged as "rc")
- test the image:rc by running a container using it
- rebuild the image, push it to the registry and promote it for use (tagged as "latest")

For this activity, you will create a new project/repo, clone the template repo and push it to your empty repo. This time the jobs in the .gitlab-ci.yaml file will have the manual setting removed, so they will run on any commit pushed to your repo.

***You will also be using a service account to perform the authentication to Docker Trusted Registry***

---

## Create a new repo in GitLab using the Docker Image Builder example repo

1. Create a new empty repository in [Force Training](https://gitlab.global.lmco.com/force/force-training)
    - name the project:
```diff
- <ntid>-docker-image-builder
```
- **NOTE:** *Ensure the main branch is not protected (Settings > Repository > Protected Branches) ensure the main branch is not protected*

### Create Sensitive Variables in GitLab

```diff
- Take note below
```
Copy the credentials located under the [template variables](https://gitlab.global.lmco.com/force/force-training/templates/docker-image-builder/-/settings/ci_cd) as you will need to use them in your repo variables</span>

2. Hover over Settings > Select CI/CD
3. Expand Variables

![CI-CD-Vars](img/git-var1.png)

4. Locate the Variables section and select *"Expand"*

![CI-CD-Vars2](img/git-var2.png)

5. Add the variables for DTR_USER and DTR_USER_SECRET

![CI-CD-usr](img/git-var-user.png)

![CI-CD-sec](img/git-var-sec.png)

6. You should see two variables set in your repo now

![CI-CD-vars3](img/git-var3.png)

---

## Clone the Template

7. Make a bare clone of the template repository under $HOME/source/

8. On your Git command line (Not CMD or Powershell), type the following commands

```bash
 cd $HOME
 cd source #this directory should exist already from Homework Week 1
 git clone --bare https://gitlab.global.lmco.com/force/force-training/templates/docker-image-builder.git
 cd docker-image-builder.git
 git push --mirror https://gitlab.global.lmco.com/force/force-training/<your-ntid>-docker-image-builder.git
 cd ..
 rm -rf docker-image-builder.git
 #Clone the new repo locally
 git clone https://gitlab.global.lmco.com/force/force-training/<your-ntid>-docker-image-builder.git
```

---

## Edit the Pipeline file

9. Open VSCode and open the training.code-workspace
10. Add the new folder to the workspace
11. Open the gitlab-ci.yml file in VSCode and edit the **three** lines replacing "\<NTID\>" with your actual NTID
    - *Example: tphensls-training-image*

![pipeline-edit](img/pipeline-edit.png)

12. Locate the manual job control and comment it out so the job will execute the pipeline on a commit being pushed to the repo

![job-control](img/pipeline-edit2.png)

13. Commit the changes and push the changes to the repo in GitLab

---

## Check the status of the job in the CI/CD console

14. In your web browser, check your Gitlab repo to ensure that the files pushed to the repo. You should see the last update time on the affected files as well as your commit message

15. In your Gitlab repo Hover over CI/CD and select "Pipelines"

![view-job](img/job-name.png)

Since the job is not set to **when: manual** in the .gitlab-ci.yml file the pipeline should be running automatically from the change pushed to the repo

16. Select the running pipeline to drill into the jobs

![view-job](img/pipeline-running.png)

17. Select the running job name to drill into the job and view the output log

![view-job](img/job-running.png)

18. You should see the docker engine executing a pull using the Docker Compose image to be run in a container

![job-run](img/job-compose.png)

19. Your jobs (build-rc, test, promote) should complete within 30 minutes

![job-complete](img/job-completed.png)

---

## Login to Enterprise Docker Trusted Registry (DTR)

[Docker Trusteed Registry](https://registry.global.lmco.com)

20. Use the credentials located under the [template variables](https://gitlab.global.lmco.com/force/force-training/templates/docker-image-builder/-/settings/ci_cd)

![DTR Login](img/dtr1.png)

21. Select the eo-force-training Organization in DTR

![DTR Org](img/dtr2.png)

22. Select the Repositories Tab
    - You should see the below image *(there may be additional repos added from the timestamp this screenshot was taken)*

![DTR Repos](img/dtr3.png)

23. Select the Image name your pipeline created \<ntid>-force-training

![Harbor Profile](img/dtr4.png)

24. Under settings, make the image public and save the changes
    - this will allow simpler definition in other pipelines to pull the image

![Harbor Profile](img/dtr5.png)

25. Select the Tags tab and take note that there are two tags
    - rc = release candidate
    - latest = promoted image

![Image Tags](img/image-tags.png)

---

## Time for the real challenge

1. Using VSCode, edit the Dockerfile to incorporate something that you would need (CLI tool, package, etc)
    - If you are adding a script to install a binary:
        - Create a directory in the root of the repo named after the tool
        - Add the binary as well as an installation script into the directory
        - Edit the Dockerfile
            - at the top append your directory to add to the build
                - ADD \<**directory**>/tmp/\<**directory**>
            - at the bottom append the call to run the script to install the binary
                - RUN bash /tmp/\<**product**>/\<**product**>-install.sh
    - If you are simply adding a linux package:
        - edit the build/install-packages.sh script
        - locate the packages array and add your package name into the list
2. Commit the changes to the repo and run the pipeline again

3. Complete the challenge by editing your first week's homework to pull your newly built image
4. Using VSCode, open the \<ntid>-gitops-pipeline repo
5. Edit the .gitlab-ci.yml file to pull your image
6. Uncomment the IMAGE: line at the time of the file by removing the # and the space after it.
     - the hot-key for this in VSCode is Ctrl+/

![image-line](img/image-line.png)

7. replace the image path:tag with your image path (do not copy the "docker pull") command (this can be found in DTR under the Info tab)

![Image Pull](img/image-pull.png)

8. Commit and push your changes to the repo and run the \<ntid>-gitops-pipeline
9. This time, you should see that the job pulls your newly created image
