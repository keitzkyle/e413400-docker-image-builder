set -e -x

# curl -O https://bootstrap.pypa.io/get-pip.py
curl -O https://bootstrap.pypa.io/pip/2.7/get-pip.py
python get-pip.py
#export PATH=~/.local/bin:$PATH
#pip3 install awscli --upgrade --user
pip install --upgrade awscli