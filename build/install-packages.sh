set -e -x

source /etc/lsb-release

function apt_get() {
  apt-get -y --allow-change-held-packages --allow-remove-essential --allow-downgrades --allow-unauthenticated --no-install-recommends "$@"
}

function install_mysql_so_files() {
    mysqlpath="/usr/lib/x86_64-linux-gnu"
    if [ "`uname -m`" == "ppc64le" ]; then
        mysqlpath="/usr/lib/powerpc64le-linux-gnu"
    fi
    if [ "`uname -m`" == "armv7l" ]; then
        mysqlpath="/usr/lib/arm-linux-gnueabihf"
    fi
    apt_get install libmysqlclient-dev
    tmp=`mktemp -d`
    mv $mysqlpath/libmysqlclient* $tmp
    apt_get remove libmysqlclient-dev libmysqlclient18
    mv $tmp/* $mysqlpath/
}

arch="amd64"
if [ "`uname -m`" == "armv7l" ]; then
    arch="armhf"
fi

packages="
aptitude
awscli
autoconf
bc
bison
build-essential
bzr
ca-certificates
cmake
csvtool
curl
dconf-gsettings-backend
dnsutils
fakeroot
flex
fuse-emulator-utils
gdb
git-core
gnupg-curl
gsfonts
imagemagick
iputils-arping
jq
krb5-user
laptop-detect
libaio1
libatm1
libcurl4-openssl-dev
libcwidget3v5
libdirectfb-1.2-9
libdrm-intel1
libdrm-nouveau2
libdrm-radeon1
libfuse-dev
libgd2-noxpm-dev
libgmp-dev
libgpm2
libgtk-3-0
libicu-dev
liblapack-dev
libmagickwand-dev
libncurses5-dev
libopenblas-dev
libpango1.0-0
libparse-debianchangelog-perl
libpq-dev
libreadline6-dev
libsasl2-dev
libsasl2-modules
libselinux1-dev
libsigc++-2.0-0v5:"$arch"
libsqlite0-dev
libsqlite3-dev
libsysfs2
libxapian22v5
libxcb-render-util0
libxslt1-dev
libyaml-dev
lsof
lzma
manpages-dev
mercurial
mutt
nodejs
nodejs-legacy
npm
ocaml-base-nox
openjdk-8-jre
openssh-server
psmisc
python3
python3-pip
quota
rsync
sendmail
sharutils
smbclient
sshfs
strace
subversion
sysstat
tasksel
tasksel-data
tcpdump
traceroute
ttf-dejavu-core
tzdata
unzip
uuid-dev
wget
zip
"

if [ "`uname -m`" == "ppc64le" ] || [ "`uname -m`" == "armv7l" ]; then
packages=$(sed '/\b\(libopenblas-dev\|libdrm-intel1\|dmidecode\)\b/d' <<< "${packages}")
ubuntu_url="http://ports.ubuntu.com/ubuntu-ports"
else
ubuntu_url="http://archive.ubuntu.com/ubuntu"
fi

cat > /etc/apt/sources.list <<EOS
deb $ubuntu_url $DISTRIB_CODENAME main universe multiverse
deb $ubuntu_url $DISTRIB_CODENAME-updates main universe multiverse
deb $ubuntu_url $DISTRIB_CODENAME-security main universe multiverse
EOS
apt-get clean
apt_get update
apt_get dist-upgrade
# TODO: deprecate libmysqlclient
install_mysql_so_files
apt_get install $packages ubuntu-minimal
echo "jay5"
apt-get clean
echo "jay6"
rm -rf /usr/share/doc/* /usr/share/man/* /usr/share/groff/* /usr/share/info/* /usr/share/lintian/* /usr/share/linda/*
##libavcodec54
#libsigc++-2.0-0c2a:"$arch"
#libxapian22
#libcwidget3
#libboost-iostreams1.54.0:"$arch"
#libept1.4.12:"$arch"
#libmariadbclient-dev